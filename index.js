//Express Setup

const express = require("express");

// Mongoose is a package that allows creation of Schemas to model our data stracture
const mongoose = require("mongoose");

const app = express();
const port = 3000;

//  [Section] - Mongoose Connection
// Mongoose uses the 'connect' function to connect to the cluster in our mongoDb Atlas

/*
	It takes 2 argument
	1. Connection string from our MongoDb Atlas.
	2. Object that contains the middlewares/standards that MondoDb uses.
*/

mongoose.connect(`mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/S35-Activiy?retryWrites=true&w=majority`, {
	// it allows us to avoid any current and/or future error while connecting to MongoDb
	useNewUrlParser: true,

	// If "false" by default. Set to true to opt in to using the MongoDb driver's new connection  management engine
	useUnifiedTopology: true
})

// Initializes the mongoose connection to the MongoDb Db by assigning 'mongoose.connection' to the 'db' variable.
let db= mongoose.connection

// Listen to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console based of the event (error or success)
db.on('error', console.error.bind(console,'connection'));
db.on('open',() => console.log('Connection to mongodb!'));



// Creating A Schema
const taskSchema = new mongoose.Schema({
	// Define the fielsd with thier corresponding data types
	// For task, it needs "task name" and it's data type will be "String"
	name: String,
	//There is a field called "status" that has a data type of "String" and the dafault value is "pending"
	status: {
		type: String,
		default: 'Pending'
	}
});


// MODELS
// The variable "Task" can now be used to run commands for interacting with our DB.
// "Task" is capitalized following the MVC approach for the naming conventions
// Models must be singular form and capitalized
// The first parameter is used to specify the Name of the collection where we will store our data
// The second parameter is used to specify the Schema/Blueprint of the documents that will be stored in our MongoDb Collection
const Task = mongoose.model('Task', taskSchema);



// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.post('/tasks',(req, res) => {
	// bussiness logic
	Task.findOne({name:req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name){
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task({
				name: req.body.name,
				status: req.body.status
			})

			newTask.save((error, savedTask)=> {
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send('New Task Created!')
				}
			})
		}
	})
})

// Get all users from collection - get all users route

app.get('/tasks', (req, res) => {
	Task.find({},(error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				task: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port: ${port}`))