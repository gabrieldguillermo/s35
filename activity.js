
const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3000;


mongoose.connect(`mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/S35-Activiy?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection

db.on('error', console.error.bind(console,'connection'));
db.on('open',() => console.log('Connection to mongodb!'));


const userSchema = new mongoose.Schema({
	username:String,
	password: String
});

const User = mongoose.model('User', userSchema);



app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.post('/register-users',(req, res) => {
	// bussiness logic
	User.findOne({username:req.body.username}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.username == req.body.username){
			return res.send('Duplicate task found')
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((error, savedTask)=> {
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send('New User Created!')
				}
			})
		}
	})
})

app.get('/users', (req, res) => {
	User.find({},(error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				user: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port: ${port}`))